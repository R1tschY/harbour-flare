extern crate libsignal_protocol_sys;

use std::{mem, fmt};
use libsignal_protocol_sys::{signal_context, signal_context_create, signal_context_destroy};
use std::ffi::c_void;
use std::cell::Cell;
use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub struct SignalError(i32);

impl Error for SignalError {

}

impl Display for SignalError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let name: &'static str = match self.0 {
            0 => "SUCCESS",
            -12 => "NOMEM",
            -22 => "INVAL",
            -1000 => "UNKNOWN",
            -1001 => "DUPLICATE_MESSAGE",
            -1002 => "INVALID_KEY",
            -1003 => "INVALID_KEY_ID",
            -1004 => "INVALID_MAC",
            -1005 => "INVALID_MESSAGE",
            -1006 => "INVALID_VERSION",
            -1007 => "LEGACY_MESSAGE",
            -1008 => "NO_SESSION",
            -1009 => "STALE_KEY_EXCHANGE",
            -1010 => "UNTRUSTED_IDENTITY",
            -1011 => "VRF_SIG_VERIF_FAILED",
            -1100 => "INVALID_PROTO_BUF",
            -1200 => "FP_VERSION_MISMATCH",
            -1201 => "FP_IDENT_MISMATCH",
            _ => "UNKNOWN_ERROR_CODE"
        };

        write!(f, "SignalError({})", name)
    }
}

pub fn signal_result(result: i32) -> SignalResult<()> {
    if result == 0 {
        Ok(())
    } else {
        Err(SignalError(result))
    }
}

pub type SignalResult<T> = Result<T, SignalError>;


pub struct SignalContext(*mut signal_context);

impl SignalContext {
    pub fn new(user_data: *mut c_void) -> SignalResult<Self> {
        unsafe {
            let mut global_context: *mut signal_context = mem::zeroed();
            signal_result(signal_context_create(&mut global_context, user_data))?;
            // TODO: check zero pointer
            Ok(SignalContext(global_context))
        }
    }

    // TODO:
    // signal_context_set_crypto_provider
    // signal_context_set_locking_functions
    // signal_context_set_log_function
}

impl Drop for SignalContext {
    fn drop(&mut self) {
        unsafe {
            signal_context_destroy(self.0)
        }
    }
}

fn main() {
    let user_data: *mut c_void = unsafe { mem::zeroed() };
    let global_context = SignalContext::new(user_data);

    println!("Hello, world!");
}