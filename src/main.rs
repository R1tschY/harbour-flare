extern crate libsignal_protocol;

use libsignal_protocol::Context;
use libsignal_protocol as signal;
use std::time::SystemTime;
use failure::Error;

fn main() -> Result<(), Error> {
    let ctx = Context::default();

    let extended_range = 0;
    let start = 123;
    let pre_key_count = 20;

    let identity_key_pair = signal::generate_identity_key_pair(&ctx)?;
    let signed_pre_key = signal::generate_signed_pre_key(
        &ctx,
        &identity_key_pair,
        5,
        SystemTime::now(),
    )?;
    println!(
        "Signed pre key ID: {} at {:?}",
        signed_pre_key.id(),
        signed_pre_key.timestamp()
    );

    let registration_id = signal::generate_registration_id(&ctx, extended_range)?;
    println!("Registration ID: {}", registration_id);

    let pre_keys = signal::generate_pre_keys(&ctx, start, pre_key_count)?;

    let pre_key_ids: Vec<_> = pre_keys
        .iter()
        .map(|session_key| session_key.id())
        .collect();

    println!("Pre Key session IDs:");
    println!("{:?}", pre_key_ids);

    Ok(())
}