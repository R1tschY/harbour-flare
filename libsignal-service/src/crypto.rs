

pub struct UnidentifiedAccess {
    access_key: Vec<u8>,
    certificate: Vec<u8>,
}

impl UnidentifiedAccess {
    pub fn get_access_key(&self) -> &[u8] { &self.access_key }
    pub fn get_certificate(&self) -> &[u8] { &self.certificate }
}