use crate::push_service::PushService;
use config::SignalServiceConfig;
use credentials::CredentialsProvider;

pub struct AccountManager {
    push: PushService,
    user: String,
}

impl AccountManager {
    pub fn new(config: SignalServiceConfig, credentials: Box<dyn CredentialsProvider>, user_agent: &str) -> Self {
        let user = credentials.get_user().to_string();
        AccountManager {
            push: PushService::new(config, credentials, user_agent),
            user
        }
    }
}