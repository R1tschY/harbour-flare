pub trait CredentialsProvider {
    fn get_user(&self) -> &str;
    fn get_password(&self) -> &str;
}