use config::{SignalServiceConfig, SignalUrl};
use credentials::CredentialsProvider;
use std::time::Duration;
use rand::thread_rng;
use rand::seq::SliceRandom;
use reqwest::{StatusCode, Method};
use reqwest::{Error as HttpError};
use reqwest::async::{Client as HttpClient};
use reqwest::async::{ClientBuilder as HttpClientBuilder};
use reqwest::async::{RequestBuilder as HttpRequestBuilder};
use reqwest::async::{Response as HttpResponse};
use crypto::UnidentifiedAccess;
use futures::Future;
use std::ops::Deref;

pub struct PushService {
    credentials: Box<dyn CredentialsProvider>,
    user_agent: String,
    service_clients: Vec<ServiceClient>
}

#[derive(Debug)]
pub enum PushServiceError {
    CaptchaRequired,
    RateLimit,
    AuthorizationFailed,
    NotFound,
    ExpectationFailed,
    NonSuccessfulResponseCode,
    MismatchedDevices,
    StaleDevices,
    DeviceLimitExceeded,
    Locked,
    NetworkError(reqwest::Error)
}

impl From<reqwest::Error> for PushServiceError {
    fn from(error: reqwest::Error) -> Self {
        PushServiceError::NetworkError(error)
    }
}

type PushServiceResult<T> = Result<T, PushServiceError>;
type PushServiceFuture<T> = Box<dyn Future<Item=T, Error=PushServiceError>>;
type PushServiceResponseFuture = PushServiceFuture<HttpResponse>;

struct ConnectionHolder {
    pub client: HttpClient,
    pub url: String,
}

struct ServiceClient {
    pub identified_client: HttpClient,
    pub unidentified_client: HttpClient,
    pub url: String,
}

impl PushService {
    const CREATE_ACCOUNT_SMS_PATH   :&'static str = "/v1/accounts/sms/code/%s?client=%s";
    const CREATE_ACCOUNT_VOICE_PATH :&'static str = "/v1/accounts/voice/code/%s";
    const VERIFY_ACCOUNT_CODE_PATH  :&'static str = "/v1/accounts/code/%s";
    const REGISTER_GCM_PATH         :&'static str = "/v1/accounts/gcm/";
    const TURN_SERVER_INFO          :&'static str = "/v1/accounts/turn";
    const SET_ACCOUNT_ATTRIBUTES    :&'static str = "/v1/accounts/attributes/";
    const PIN_PATH                  :&'static str = "/v1/accounts/pin/";

    const PREKEY_METADATA_PATH      :&'static str = "/v2/keys/";
    const PREKEY_PATH               :&'static str = "/v2/keys/%s";
    const PREKEY_DEVICE_PATH        :&'static str = "/v2/keys/%s/%s";
    const SIGNED_PREKEY_PATH        :&'static str = "/v2/keys/signed";

    const PROVISIONING_CODE_PATH    :&'static str = "/v1/devices/provisioning/code";
    const PROVISIONING_MESSAGE_PATH :&'static str = "/v1/provisioning/%s";
    const DEVICE_PATH               :&'static str = "/v1/devices/%s";

    const DIRECTORY_TOKENS_PATH     :&'static str = "/v1/directory/tokens";
    const DIRECTORY_VERIFY_PATH     :&'static str = "/v1/directory/%s";
    const DIRECTORY_AUTH_PATH       :&'static str = "/v1/directory/auth";
    const DIRECTORY_FEEDBACK_PATH   :&'static str = "/v1/directory/feedback-v3/%s";
    const MESSAGE_PATH              :&'static str = "/v1/messages/%s";
    const SENDER_ACK_MESSAGE_PATH   :&'static str = "/v1/messages/%s/%d";
    const UUID_ACK_MESSAGE_PATH     :&'static str = "/v1/messages/uuid/%s";
    const ATTACHMENT_PATH           :&'static str = "/v2/attachments/form/upload";

    const PROFILE_PATH              :&'static str = "/v1/profile/%s";

    const SENDER_CERTIFICATE_PATH   :&'static str = "/v1/certificate/delivery";


    pub fn new(config: SignalServiceConfig, credentials: Box<dyn CredentialsProvider>, user_agent: &str) -> Self {
        let timeout = Duration::from_secs(30);
        PushService {
            credentials,
            user_agent: user_agent.to_string(),
            service_clients: Self::create_service_clients(
                config.get_service_urls(),
                user_agent,
                timeout),
        }
    }

    pub fn request_sms_verification_code(&self, captcha_token: Option<String>) -> PushServiceFuture<()> {
        let captcha_query = match captcha_token {
            None => String::new(),
            Some(token) => format!("&captcha={}", token)
        };

        let path = format!(
            "/v1/accounts/sms/code/{}?client={}{}",
            self.credentials.get_user(),
            "android",
            captcha_query
        );

        let request = self.create_service_request(Method::GET, &path, None);

        let future = self.send_service_request(request);
        Box::new(
            future
                .and_then(|response| {
                    if response.status() == StatusCode::PAYMENT_REQUIRED {
                        Err(PushServiceError::CaptchaRequired)
                    } else {
                        Ok(response)
                    }
                })
                .and_then(|response| Self::handle_response(response))
                .map(|_| ())
        )
    }

    fn create_service_request(
        &self,
        method: reqwest::Method,
        path: &str,
        unidentified_access: Option<UnidentifiedAccess>
    ) -> HttpRequestBuilder {
        let service_client = self.get_service_client();
        let client = match unidentified_access {
            None => &service_client.identified_client,
            Some(_) => &service_client.unidentified_client
        };

        let builder = client
            .request(method, &format!("{}{}", service_client.url, path));

        let builder = if let Some(unid_access) = unidentified_access {
            builder.header(
                "Unidentified-Access-Key", base64::encode(unid_access.get_access_key()))
        } else {
            builder.header(
                "Authorization", Self::get_authorization_header(self.credentials.deref()))
        };

        // TODO: add to create_client
        if !self.user_agent.is_empty() {
            builder.header("X-Signal-Agent", self.user_agent.clone())
        } else {
            builder
        }
    }

    fn handle_response(
        response: HttpResponse
    ) -> PushServiceResult<HttpResponse> {
        let status = response.status();
        match status {
            StatusCode::PAYLOAD_TOO_LARGE =>
                Err(PushServiceError::RateLimit),
            StatusCode::UNAUTHORIZED | StatusCode::FORBIDDEN =>
                Err(PushServiceError::AuthorizationFailed),
            StatusCode::NOT_FOUND =>
                Err(PushServiceError::NotFound),
            StatusCode::CONFLICT => {
//                    MismatchedDevices mismatchedDevices;
//
//                    try {
//                        mismatchedDevices = JsonUtil.fromJson(responseBody, MismatchedDevices.class);
//                    } catch (JsonProcessingException e) {
//                        Log.w(TAG, e);
//                        throw new NonSuccessfulResponseCodeException("Bad response: " + responseCode + " " + responseMessage);
//                    } catch (IOException e) {
//                        throw new PushNetworkException(e);
//                    }
//
//                    throw new MismatchedDevicesException(mismatchedDevices);
                Err(PushServiceError::MismatchedDevices)
            },
            StatusCode::GONE => {
//                    StaleDevices staleDevices;
//
//                    try {
//                        staleDevices = JsonUtil.fromJson(responseBody, StaleDevices.class);
//                    } catch (JsonProcessingException e) {
//                        throw new NonSuccessfulResponseCodeException("Bad response: " + responseCode + " " + responseMessage);
//                    } catch (IOException e) {
//                        throw new PushNetworkException(e);
//                    }
//
//                    throw new StaleDevicesException(staleDevices);
                Err(PushServiceError::StaleDevices)
            },
            StatusCode::LENGTH_REQUIRED => {
//                    DeviceLimit deviceLimit;
//
//                    try {
//                        deviceLimit = JsonUtil.fromJson(responseBody, DeviceLimit.class);
//                    } catch (JsonProcessingException e) {
//                        throw new NonSuccessfulResponseCodeException("Bad response: " + responseCode + " " + responseMessage);
//                    } catch (IOException e) {
//                        throw new PushNetworkException(e);
//                    }
//
//                    throw new DeviceLimitExceededException(deviceLimit);
                Err(PushServiceError::DeviceLimitExceeded)
            },
            StatusCode::EXPECTATION_FAILED => Err(PushServiceError::ExpectationFailed),
            StatusCode::LOCKED => {
//                    RegistrationLockFailure accountLockFailure;
//
//                    try {
//                        accountLockFailure = JsonUtil.fromJson(responseBody, RegistrationLockFailure.class);
//                    } catch (JsonProcessingException e) {
//                        Log.w(TAG, e);
//                        throw new NonSuccessfulResponseCodeException("Bad response: " + responseCode + " " + responseMessage);
//                    } catch (IOException e) {
//                        throw new PushNetworkException(e);
//                    }
//
//                    throw new LockedException(accountLockFailure.length, accountLockFailure.timeRemaining);
                Err(PushServiceError::Locked)
            },
            _ if status != StatusCode::OK && status != StatusCode::NO_CONTENT =>
                Err(PushServiceError::NonSuccessfulResponseCode),
            _ => Ok(response),
        }
    }

    fn send_service_request(
        &self, request: HttpRequestBuilder
    ) -> impl Future<Item=HttpResponse, Error=PushServiceError> {
        let future = request.send();
        // TODO: connections.add(future)

        future.map_err(|err| PushServiceError::NetworkError(err))
    }

    fn make_service_request(
        &self, request: HttpRequestBuilder
    ) -> PushServiceResponseFuture {
        let future = self.send_service_request(request);
        Box::new(future.and_then(Self::handle_response))
    }

    fn get_service_client(&self) -> &ServiceClient {
        let mut rng = thread_rng();
        self.service_clients.choose(&mut rng).unwrap()
    }

    fn create_service_clients(
        urls: &[SignalUrl], user_agent: &str, timeout: Duration
    ) -> Vec<ServiceClient> {
        urls.iter().map(|url| {
            ServiceClient::new(
                Self::create_client(user_agent, timeout),
                Self::create_client(user_agent, timeout),
                url.get_url()
            )
        }).collect()
    }

    fn create_client(
        user_agent: &str, timeout: Duration
    ) -> HttpClient {
        HttpClientBuilder::new()
            .use_default_tls()
            .timeout(timeout)
            .build()
            .expect("PushService::create_client()")
    }

    fn get_authorization_header(credentials: &dyn CredentialsProvider) -> String {
        let creds = format!("{}:{}", credentials.get_user(), credentials.get_password());
        format!("Basic {}", base64::encode(creds.as_bytes()))
    }
}

impl ServiceClient {
    pub fn new(
        identified_client: HttpClient,
        unidentified_client: HttpClient,
        url: &str
    ) -> Self {
        ServiceClient {
            identified_client,
            unidentified_client,
            url: url.to_string()
        }
    }
}