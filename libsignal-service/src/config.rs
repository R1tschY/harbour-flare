
#[derive(Clone, PartialOrd, PartialEq)]
pub struct SignalUrl {
    url: String
}

pub struct SignalServiceConfig {
    service_urls: Vec<SignalUrl>
}

impl SignalServiceConfig {
    pub fn new(service_urls: &[SignalUrl]) -> Self {
        SignalServiceConfig {
            service_urls: service_urls.to_vec()
        }
    }

    pub fn get_service_urls(&self) -> &[SignalUrl] {
        &self.service_urls
    }
}

impl SignalUrl {
    pub fn get_url(&self) -> &str {
        &self.url
    }
}